#!/usr/bin/env python3
#
# Tool to automatically check version and update EDID in HDMI receiver.
#

from hdktool import HDKTool, logger
#import sys
#import time

def main():
    logger.info("HDMI Update automated utility")
    h = HDKTool()
    with h:
        print("OSVR HDK - HDMI Update automated utility")
        h.infoPrint("Connecting to HDK")
        h.do_connect("")

        if not h.is_connected():
            h.infoPrint("HDK serial port not detected or unable to connect - exiting.")
            h.do_quit("")
            return

        h.infoPrint("Getting firmware version")
        h.do_getver("")
        h.infoPrint("Re-initializing HDMI receiver")
        h.execAndAwaitLine("hi", ";End init") # Always has this final line.
        h.infoPrint("Re-writing HDMI EDID data")
        h.do_execAndAwaitLines("hm0", 4) # Always prints 4 lines: a Program MTP line and 3 result lines.
        h.infoPrint("Exiting.")
        h.do_quit("")


if __name__ == "__main__":
    main()
