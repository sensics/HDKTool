#!/usr/bin/env python3

from serial import SerialException, SerialTimeoutException

# Ryan Pavlik <ryan@sensics.com>

class SerialUniversalLineReader(object):
    """Utility class for reading serial line-by-line, even if the source might end its lines with LFCR instead of CRLR"""

    LF = '\n'
    CR = '\r'
    def __init__(self):
        self._bufferedChar = None
        self._line = None

    def _tryRead(self, serial):
        if self._bufferedChar is not None:
            # We had one character left over from last time that we didn't consume.
            char = self._bufferedChar
            self._bufferedChar = None
        else:
            # get a character the old fashioned way: read one.
            try:
                char = serial.read(1)
            except SerialTimeoutException:
                # Timeout waiting for character: return None.
                return None
        if char == "":
            # Timeout waiting for character: return None.
            return None
        return char

    def _appendChar(self, char):
        if self._line is None:
            self._line = [char]
        else:
            self._line.append(char)

    def _getLineAndReset(self):
        line = None
        if self._line is not None:
            line = "".join(self._line)
        self._line = None
        return line

    def readline(self, serial):
        """
        Read a line from the serial port a character at a time, coping with the fact that
        the firmware may use non-standard order \n\r end of line markers.

        If we get any end-of-line marker, we'll try (one timeout period) to consume its match,
        and you'll get a \n. If we don't get any end of line markers, you won't get any. If we
        don't get any characters, you'll get None.

        Don't pass in a serial port without a short timeout set, since we do read one character
        at a time.
        """
        gotCR = False
        gotNewline = False
        while 1:
            char = self._tryRead(serial)

            if char is None:
                # Timed out waiting for a character.
                return self._getLineAndReset()

            if char == SerialUniversalLineReader.LF:
                # Got a newline - stick \n on the string, set the flag, then get ready to eat the \r if available.
                gotNewline = True
                self._appendChar(SerialUniversalLineReader.LF)
                break
            if char == SerialUniversalLineReader.CR:
                # Got a carriage return - not expected since we usually get \n\r, but stick \n on the string and look for an \n to eat.
                gotCR = True
                self._appendChar(SerialUniversalLineReader.LF)
                break

            # Otherwise, it's just a character.
            #print("Got character", char)
            self._appendChar(char)

        # OK, if we got down here, we've got some line ending, and we'd like to try to consume its pair, if it exists.
        nextChar = self._tryRead(serial)

        if nextChar is None:
            # timeout is fine by us here, just return!
            return self._getLineAndReset()
        if gotNewline and nextChar == SerialUniversalLineReader.CR:
            # This is a matching pair: just drop this character and return the line!
            return self._getLineAndReset()
        if gotCR and nextChar == SerialUniversalLineReader.LF:
            # This is a matching pair: just drop this character and return the line!
            return self._getLineAndReset()

        # Uh oh - we got a character that isn't a matching pair. Save it for next time, then return the line.
        self._bufferedChar = nextChar

        return self._getLineAndReset()


