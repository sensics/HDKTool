#!/usr/bin/env python3
#
# Tool to interact with the HDK over the Virtual COM port
#
# Khalid Zubair <kzubair@hcrest.com>
# Ryan Pavlik <ryan@sensics.com>
#
# Needed modules:
#   pyserial
#   tabulate
#   cmd2
#   pywin32 if on Windows




from cmd import Cmd
from serial import SerialException, SerialTimeoutException
from tabulate import tabulate
from threading import Event
import itertools
import logging
import queue
import re
import serial
import serial.tools
import serial.tools.list_ports
import sys
import threading
import time

import SerialUniversalLineReader

logging.basicConfig(level=logging.DEBUG, filename="hdktool.log", filemode="at")
logger = logging.getLogger('HDKTool')
logger.setLevel(logging.DEBUG)


def spin():
    print("%s" % next(spin.i), end="\r")
spin.i = itertools.cycle('/-\|')


def _printComPorts(ports):
    print(tabulate(ports,
                   headers=["Port", "Name", "Device Path"]))


def cal_flags(inp):
    if inp == "none":
        return 0

    if re.match(".*[^agm].*", inp):
        return None

    flags = 0
    for i, c in enumerate("agm"):
        if c in inp:
            flags = flags | 1 << i

    return flags

def _cleanLine(myLine):
    if myLine is None:
        return (None, None)
    if myLine != "":
        stripped = myLine.strip()
        if not stripped:
            # then all we got was a newline
            logger.debug("recv {newline}")
        else:
            logger.debug("recv [%s]", stripped)
        return (stripped, myLine)
    return ("", "")

def printLineFromMcu(s):
    print(">>", s)

class HDKWorker(threading.Thread):

    """
        Handle commands and responses from the HDK in a background thread

        This worker thread allows the serial port operations to happen while
        the main app waits for input on stdin

        A select() or poll() based approach wouldn't have worked because both
        PySerial and the Cmd class don't allow it.

        The worker thread reads from the serial port with a short timeout (0.1s)
        and services commands from the host while waiting.
    """

    speed = 57600
    timeout = 0.1

    def __init__(self):
        threading.Thread.__init__(self)
        self._quit = False
        self._serial = None
        self._serialReader = SerialUniversalLineReader.SerialUniversalLineReader()
        self._autoconnect = False
        self._hdkPattern = r"""(%s)""" % "|".join((
            "VID:PID=1532:0B00",
            "OSVR Communication Device",
            "Communication Device Class ASF composite example 2",
            "Sensics Communication Device",
            "USB CDC Serial Device",
            "VID:PID=03EB:2421",
        ))

        self._postEvent = Event()
        self._responseEvent = Event()
        self._postCmd = None
        self._responseValue = None
        self._responseException = None
        self._gotSerialException = False

    def getComPorts(self, grep=None):
        if grep:
            return serial.tools.list_ports.grep(grep)
        return serial.tools.list_ports.comports()

    def _open(self, port, name, fp):
        logger.info("Opening port %s (%s, %s)", port, name, fp)
        self._serial = serial.Serial(port, HDKWorker.speed, timeout = HDKWorker.timeout, rtscts = True)
        return self._serial

    def _readlineNewlineCarriageReturn(self):
        return self._serialReader.readline(self._serial)

    def readlineTupleNoExcept(self):
        """read from the serial port, return (None, None) on a timeout

        The first return value is stripped, while the second, if present, is the raw return from readline.
        """

        try:
            line = self._readlineNewlineCarriageReturn()
            return _cleanLine(line)
        except SerialException as e:
            logger.debug("Got SerialException, probably just a disconnect - details follow.")
            self._gotSerialException = True
        except:
            self._gotSerialException = True
            logger.exception(sys.exc_info())
        return (None, None)

    def readlineNoExcept(self):
        """Read from the serial port, return None on a timeout, and only provide a stripped return value."""
        retTup = self.readlineTupleNoExcept()
        if retTup is None:
            return None
        return retTup[0]

    def readlineTuple(self):
        """read from the serial port, return Exception on a timeout"""
        retTup = self.readlineTupleNoExcept()
        if retTup is None or retTup[0] is None:
            raise SerialTimeoutException("Timeout reading from Serial Port in readlineTuple")
        return retTup

    def readline(self):
        """read from the serial port, return Exception on a timeout"""
        retTup = self.readlineTupleNoExcept()
        if retTup is None or retTup[0] is None:
            raise SerialTimeoutException("Timeout reading from Serial Port in readline")
        return retTup[0]

    def readtab(self, n, sep=":", chomp=True):
        rows = [self.readline().split(sep) for x in range(n)]
        if chomp:
            del rows[-1]
        return tabulate(rows)

    def checkConnected(self, quiet=False):
        if not self._serial:
            if not quiet:
                print("Not connected")
            return False

        return True

    def writeline(self, command, echoback=True):
        if not self.checkConnected():
            return False

        logger.debug("send [%s]", command)
        self._serial.write(command + "\n")
        if echoback:
            echo = self.readline()
            if echo != command:
                logger.error(
                    "command [%s] did not echo back [%s]", command, echo)
                return False

        return True

    def execAndWait(self, command, seconds, echoback=True):
        logger.debug("exec and wait [%s] [%d seconds]", command, seconds)
        endTime = time.clock() + seconds
        if not self.writeline(command, echoback):
            return False

        # Just read lines until the time elapses
        line = self.readlineNoExcept()
        while line != ">" and time.clock() < endTime:
            if line is not None:
                printLineFromMcu(line)
            line = self.readlineNoExcept()

        if line == ">":
            logger.debug("exec [%s] output > without additional newline", command)
            return True

        # Now, send another endline to get another prompt.
        self._serial.write("\n")
        while line != ">":
            if line is not None:
                printLineFromMcu(line)
            line = self.readlineNoExcept()

        logger.debug("exec [%s] got terminating >", command)
        return True

    def execAndAwaitLine(self, command, lastLine, echoback=True):
        logger.debug("exec and await [%s] - last line [%s]", command, lastLine)
        if not self.writeline(command, echoback):
            return False

        # Just read lines until we get what we want.
        while True:
            (line, unstripped) = self.readlineTupleNoExcept()
            if unstripped:
                # if there's something, anything (even a newline)
                # in the unstripped output, let's print it.
                printLineFromMcu(line)
                if line == lastLine:
                    break

        return True

    def execAndAwaitLines(self, command, lines, echoback=True):
        logger.debug("exec and await lines [%s] - number of lines [%d]", command, lines)
        if not self.writeline(command, echoback):
            return False

        # Just read lines until we get what we want.
        numLines = 0
        while True:
            (line, unstripped) = self.readlineTupleNoExcept()
            # Sometimes we get an empty line when other terminal apps don't get anything...
            if unstripped and line:
                printLineFromMcu(line)
                numLines = numLines + 1
                if numLines == lines:
                    break
        return True

    def execAndPredicate(self, command, predicate, echoback=True):
        logger.debug("exec and predicate [%s]", command)
        if not self.writeline(command, echoback):
            return False

        # Just read lines until the predicate returns true.
        predicateResult = False
        while not predicateResult:
            (line, unstripped) = self.readlineTupleNoExcept()
            if unstripped and line:
                printLineFromMcu(line)
            predicateResult = predicate(line, unstripped)
        return True

    def _findport(self, search):
        search = search.strip()
        if search == "":
            pattern = self._hdkPattern
        else:
            pattern = search

        for port, name, fp in self.getComPorts(pattern):
            # connect to the first device matching our pattern
            return port, name, fp

        return None

    def connect(self, line, quiet=False):
        if self._serial:
            print("Already connected to %s" % self._serial.name)
            return

        t = self._findport(line)
        if not t:
            if not quiet:
                if line:
                    print("`%s' is not a valid COM port. "
                          "Run lsports to get a list of ports" % line)
                else:

                    print("Did not find an HDK")
            return

        self._serial = self._open(*t)
        print("Connected to %s" % self._serial.name)

    def autoconnect(self, port):
        if self._serial:
            print("Already connected to %s" % self._serial.name)
            return

        print("Waiting for HDK. Run the 'disconnect' command to cancel")
        self._autoconnect = True
        self._autoconnectPort = port

    def disconnect(self):
        q = self._autoconnect
        self._autoconnect = False
        self._autoconnectPort = None

        if not self.checkConnected(quiet=q):
            return

        print("Disconnecting %s" % self._serial.name)
        self._serial.close()
        self._serial = None

    def quit(self):
        self._quit = True

    def post(self, fn, *args, **kwargs):
        """
            run fn on the worker thread
            task is a function that takes the HDKWorker as the first param
            followed by *args and **kwargs

            fn can be a method defined in HDKWorker or a callback defined
            elsewhere

            return whatever value fn returns
        """
        self._postCmd = (fn, args, kwargs)
        # logger.debug("posting(%s)", repr(fn))
        self._postEvent.set()
        self._responseEvent.wait()
        r = self._responseValue
        e = self._responseException
        self._responseValue = None
        self._responseException = None
        self._responseEvent.clear()
        # logger.debug("got response(%s)", repr(r))
        if e:
            raise e[1]
        return r

    def run(self):
        logger.info("Starting worker thread")
        while not self._quit:
            try:
                if self._postEvent.is_set():
                    fn, args, kwargs = self._postCmd
                    self._postCmd = None
                    self._postEvent.clear()
                    try:
                        self._responseValue = fn(self, *args, **kwargs)
                        self._responseException = None
                    except:
                        e = sys.exc_info()
                        self._responseValue = None
                        self._responseException = e
                        logger.exception(e[1])
                    self._responseEvent.set()
                    continue

                if self._serial:
                    line = self.readlineNoExcept()
                    if line:
                        logger.debug("unsolicited: [%s]", line)
                        print("unsolicited: [%s]" % line)
                    elif self._gotSerialException:
                        # Just going to assume it was a disconnect.
                        print("Disconnected %s" % self._serial.name)
                        self._serial.close()
                        self._serial = None
                        self._gotSerialException = False
                    continue

                if self._autoconnect:
                    t = self._findport(self._autoconnectPort)
                    if t:
                        port, name, fp = t
                        print("Found device at", port)
                        # wait for bus to settle
                        for x in range(25):
                            print("connecting...", end="")
                            spin()
                            time.sleep(0.1)
                        self._serial = self._open(*t)
                        print("Connected to %s" % self._serial.name)

                time.sleep(0.1)
            except:
                logger.exception(sys.exc_info()[0])
                print(sys.exc_info()[0])

        logger.info("Exiting worker thread")


class HDKTool(Cmd):

    """
        Command processor front-end
        Accepts input from stdin and dispatches to HDKWorker
    """

    prompt = 'hdktool> '
    intro = "Interactive HDK command tool."

    def __init__(self):
        Cmd.__init__(self)
        self._worker = HDKWorker()

    def _post(self, fn, *args, **kwargs):
        return self._worker.post(fn, *args, **kwargs)

    def _yesno(self, line):
        line = line.lower()
        if line in ("yes", "1", "true", "on"):
            return 1

        return 0

    def infoPrint(self, s):
        logger.info(s)
        print("\n")
        print(s)
        print("\n")

    def do_lsports(self, line):
        "lsports - list COM ports"
        _printComPorts(self._post(HDKWorker.getComPorts))

    def do_watchports(self, line):
        "watchports - like lsports, but watches for changes"
        print("Watching Serial Port connections. Press Ctrl+C to interrupt")
        prev = []
        try:
            while True:
                spin()
                ports = list(self._post(HDKWorker.getComPorts))
                if len(ports) != len(prev):
                    print("Changed!")
                    _printComPorts(ports)
                    prev = ports
                time.sleep(0.1)
        except KeyboardInterrupt:
            print("Done.")
            return

    def do_connect(self, line):
        """connect <port> - connect to an HDK. Optional <port> argument matches
                a specific port number (e.g. COM9) or description (ASF Example)
        """
        self._post(HDKWorker.connect, line)

    def do_autoconnect(self, line):
        """autoconnect <port> - like connect, but watch conncections and
                wait for device. if disconnected, waits and reconnects
        """
        print("Attempting autoconnect (experimental)")
        self._post(HDKWorker.autoconnect, line)

    def do_disconnect(self, line, quiet=False):
        "disconnect - disconnect active connection or cancel autoconnect"
        self._post(HDKWorker.disconnect)

    def do_q(self, line):
        """q - quit application"""
        return self.do_quit(line)

    def do_quit(self, line):
        """quit - quit application"""
        self._post(HDKWorker.quit)
        return True

    def do_exec(self, line):
        """exec <cmd> - send commands prefixed with # directly over
            the serial port"""
        self._post(HDKWorker.writeline, "#" + line)

    def do_execAndWait(self, line, seconds):
        """execAndWait <cmd> <seconds>- send commands prefixed with # directly over
            the serial port, waiting the given number of seconds"""
        self._post(HDKWorker.execAndWait, "#" + line, seconds)

    def execAndAwaitLine(self, line, finalLine):
        """execAndAwaitLine <cmd> <finalLine>- send commands prefixed with # directly over
            the serial port, waiting until the final line is received"""
        self._post(HDKWorker.execAndAwaitLine, "#" + line, finalLine)

    def do_execAndAwaitLines(self, line, lines):
        """execAndAwaitLines <cmd> <lines>- send commands prefixed with # directly over
            the serial port, waiting for the given number of non-empty response lines"""
        self._post(HDKWorker.execAndAwaitLines, "#" + line, lines)

    def do_bno_stats(self, line):
        """bno_stats - get BNO stats (run #BSQ)"""
        def get_stats(worker):
            if not worker.checkConnected():
                return
            worker.writeline("#BSQ")
            print(worker.readtab(4))

        self._post(get_stats)

    def do_bno_debug(self, line):
        """bno_stats - get BNO stats (run #BSQ)"""
        enabled = self._yesno(line)

        def f(worker):
            if not worker.checkConnected():
                return
            worker.writeline("#BVV%02x" % enabled)

        self._post(f)

    def do_mag(self, line):
        """mag <on|off> - turn mag sensor output on or off"""
        enabled = self._yesno(line)

        def f(worker):
            if not worker.checkConnected():
                return
            worker.writeline("#BME%02x" % enabled)
            print(">>", worker.readline())

        self._post(f)

    def do_magstatus(self, line):
        """magstatus [-l] - report mag status, loop if -l is given"""

        loop = False
        if line == "-l":
            loop = True
        elif line:
            print("Invalid args[%s]", line)
            return

        def f(worker):
            if not worker.checkConnected():
                return
            worker.writeline("#BMQ")
            text = worker.readline()
            label, mid, val = text.partition(":")
            if label == "Mag Accuracy":
                accuracies = {
                    "255": "unavailable",
                    "0": "Unreliable",
                    "1": "Accuracy low",
                    "2": "Accuracy medium",
                    "3": "Accuracy high",
                }
                val = val.strip()
                if val in accuracies:
                    print(">> %s: %10s" % (label, accuracies[val]))
                    return True
            print("Unexpected response [%s]" % text)
            return False

        while True:
            try:
                if not self._post(f) or not loop:
                    break
                time.sleep(0.25)
            except KeyboardInterrupt:
                print("Done.")
                return

    def do_calen(self, line):
        """\
        calen <flags>|none - combination of a,g,m to enable acc, gyro and mag
        cal or none to turn all off
        e.g. calen ag - calibrate acc and gyro"""

        line = line.lower()
        flags = cal_flags(line)
        if flags is None:
            print("invalid calen flags: %s" % line)
            return

        print(">>", "sending flags=%02x" % flags)

        def f(worker):
            if not worker.checkConnected():
                return
            worker.writeline("#BDE%02x" % flags)
            print(">>", worker.readline())

        self._post(f)

    def do_getver(self, line):
        """getver - get HDK SW version"""
        def f(worker):
            if not worker.checkConnected():
                return
            # Just execute and the command and print whatever you get for the next second.
            worker.execAndWait("#?V", 1)

        self._post(f)

    def do_calibrate(self, line):
        """calibrate - put the HDK in to factory calibration mode"""

        def start(worker):
            if not worker.checkConnected():
                return False
            print(">> Starting calibration")

            # enable calibration on all sensors
            worker.writeline("#BDE%02x" % cal_flags("agm"))
            print(">>", worker.readline())

            # turn on mag
            worker.writeline("#BME01")
            print(">>", worker.readline())
            return True

        if not self._post(start):
            return

        input(
            ">> Perform Accel and Gyro calibration. ""Press ENTER when done")

        print(">> Waiting for Mag to calibrate. Press Ctrl+C to cancel")
        status = None
        cal_level = 0
        try:
            while True:

                def get_status(worker):
                    if not worker.checkConnected():
                        return None

                    worker.writeline("#BMQ")
                    return worker.readline()

                text = self._post(get_status)

                if text is None:
                    print("Lost connection")
                    return

                label, mid, val = text.partition(":")
                val = int(val)
                if val > 3:
                    print(">> invalid accuacy value[%d]" % val)
                    return
                A = ["Unreliable", "Low", "Medium", "High"]
                accuracy = A[val]
                print("Accuracy=%-10s" % A[val], end="")
                spin()

                if val > cal_level:
                    print("")
                    cal_level = val
                    if val == 2:  # medium
                        print("!! Medium calibration, press Ctrl+C to accept")
                    elif val == 3:
                        print("!! Mag calibration complete")
                        time.sleep(0.5)
                        break

                time.sleep(0.25)
        except KeyboardInterrupt:
            print("")
            if cal_level < 2:
                print("Cancelled")
                return

        def finalize(worker):
            print(">> Finalizing")

            # save DCD
            worker.writeline("#BDS")
            print(">>", worker.readline())

            # restore default config
            worker.writeline("#BRI")
            print(">>", worker.readline())

        self._post(finalize)

    def do_bno_reset(self, line):
        """bno_reset - reset the bno"""
        def f(worker):
            if not worker.checkConnected():
                return
            worker.writeline("#BRH")
            print(">>", worker.readline())

        self._post(f)

    def do_bno_reinit(self, line):
        """bno_reinit - reinit the bno (discard any sensor on/off
        or cal flags)"""
        def f(worker):
            if not worker.checkConnected():
                return
            worker.writeline("#BRI")
            print(">>", worker.readline())

        self._post(f)

    def do_shell(self, line):
        """! - run commands prefixed with ! directly"""
        def shell(worker):
            if not worker.checkConnected():
                return
            worker.writeline(line)

        self._post(shell)

    def do_bootloader(self, line):
        """bootloader - reset the MCU into bootloader mode"""
        def f(worker):
            if not worker.checkConnected():
                return

            try:
                # We expect to be disconnected - but we do actually get echo back usually.
                worker.writeline("#?B1948")
            except SerialTimeoutException:
                # This is a harmless exception here, since we expect to be disconnected.
                pass

        self._post(f)

    def is_connected(self):
        def f(worker):
            if not worker.checkConnected():
                return False
            return True
        return self._post(f)

    def emptyline(self):
        """do nothing on an empty line (override to prevent repeating
        command)"""
        pass

    def __enter__(self):
        self._worker.start()

    def __exit__(self, type, value, traceback):
        self._worker.quit()


def main():
    h = HDKTool()
    with h:
        h.cmdloop()

if __name__ == "__main__":
    main()
