if [%PYTHON2_DIR%] == [] set PYTHON2_DIR=c:\tools\Python2
pushd %~dp0

rem Build the basic toolset
set OUTDIR=hdktool
call :build_tools
call :package_tools

rem Build the full toolset that includes the unrestricted hdktool
set OUTDIR=hdktool-full
call :build_tools
python "%PYTHON2_DIR%\Scripts\cxfreeze" hdktool.py --install-dir=%OUTDIR% -c --icon=HDK-repair.ico
call :package_tools

pause
popd
goto :eof

:build_tools
del /s /q %OUTDIR%\*.*
del %OUTDIR%.zip

python "%PYTHON2_DIR%\Scripts\cxfreeze" enter-bootloader.py --install-dir=%OUTDIR% -c --icon=HDK-bootloader.ico

python "%PYTHON2_DIR%\Scripts\cxfreeze" hdmi-update.py --install-dir=%OUTDIR% -c --icon=HDK-repair.ico

python "%PYTHON2_DIR%\Scripts\cxfreeze" get-firmware-version.py --install-dir=%OUTDIR% -c --icon=HDK-repair.ico
goto :eof

:package_tools
7za a %OUTDIR%.zip %OUTDIR%
goto :eof
