#!/usr/bin/env python3
#
# Tool to automatically enter bootloader mode.
#

from hdktool import HDKTool, logger
import time

def main():
    logger.info("OSVR HDK bootloader entry")
    h = HDKTool()
    with h:
        print("OSVR HDK - bootloader automated utility")
        h.infoPrint("Connecting to HDK")
        h.do_connect("")

        if not h.is_connected():
            h.infoPrint("HDK serial port not detected or unable to connect - exiting.")
            h.do_quit("")
            return

        h.infoPrint("Getting firmware version")
        h.do_getver("")
        h.infoPrint("Sending command to switch to bootloader mode - USB devices will disappear and re-appear.")
        h.do_bootloader("")
        h.infoPrint("Bootloader command sent - standard interface will disconnect as we wait a few seconds to allow the bootloader interface to be detected.")
        h.do_quit("")


if __name__ == "__main__":
    main()
    # Sleep required to let Windows re-detect the device as its DFU interface.
    time.sleep(5)
    logger.debug("Completed sleep time to permit DFU interface detection.")
