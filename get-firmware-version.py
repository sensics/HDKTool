#!/usr/bin/env python3
#
# Tool to check firmware version and stay open until the user confirms.
#

from hdktool import HDKTool, logger

def main(scripted):
    logger.info("OSVR HDK - Get Firmware Version utility")
    h = HDKTool()
    with h:
        print("OSVR HDK - Get Firmware Version utility")
        h.infoPrint("Connecting to HDK")
        h.do_connect("")

        if not h.is_connected():
            h.infoPrint("HDK serial port not detected or unable to connect.")
            if not scripted:
                input("Press enter to continue...")

            h.do_quit("")
            return

        h.infoPrint("Getting firmware version")
        h.do_getver("")
        if not scripted:
            input("Press enter to continue...")

        h.infoPrint("Exiting.")
        h.do_quit("")

usageString = """
%s [option]

where option may be one of the following:

    --scripted  do not prompt for an enter press before exiting

    -h
    --help
    /?          show this usage information.
"""

if __name__ == "__main__":
    import sys
    script = False
    if len(sys.argv) > 1:
        if sys.argv[1] == "--scripted":
            # scripted mode
            script = True
        if "-h" in sys.argv or "--help" in sys.argv or "/?" in sys.argv or len(sys.argv) > 2:
            # Too many args, or a help arg
            print((usageString % sys.argv[0]))
            sys.exit(1)

    main(script)
