# HDKTool

HDKTool is a CLI to the serial port interface on the HDK.

## Requisites

To run hdktool you need Python 3 and several Python libraries. See requirements.txt for a list of libs and use pip to install the libs.

```cmd
pip3 install -r requirements.txt
```

On debian, these are the packages:

```sh
sudo apt install python3-serial python3-tabulate python3-cmd2
```

### Setting up pip on Windows

Download and run the get-pip script to install pip on Windows.
https://bootstrap.pypa.io/get-pip.py

Depending on how Python is installed, you might not have Python or pip in your PATH. Add C:\Python35 and C:\Python35\Scripts to your PATH.

References:
 [ 1 ] https://pip.pypa.io/en/latest/installing.html

## Make a portable release

Use cxfreeze to create a portable release of hdktool. See below for an example cxfreeze invocation

```cmd
python c:\python35\scripts\cxfreeze hdktool.py -c --install-dir=c:\temp\hdktool
```
